# BAKGAME Game Repository
*German version below*

This is the BAKGame game repository. BAKGame is an IT security 
training project optimized for small and medium-sized businesses that is founded
by the German BMWK. The idea of BAKGame is to provide games, that teach
IT security awareness and a sensibility for threats by gamification. Within this repository we provide
the source code of our games. Further information about BAKGame and
a hosted version of the games can be found on our website https://bakgame.de .

The source code in this repository is free software, it is licensed under
the **AGPLv3**, for more information according the license please consider the
`LICENSE.md`. In order to contribute please don't hesitate to open tickets
or a merge request with your changes.

If you have any questions or feedback according the code itself or other aspects
of the project feel free to contact us by mail: info@bakgame.de

---
Dies ist das BAKGame-Spiele-Repository. 
BAKGame ist ein IT-Trainingsprojekt, welches speziell auf KMUs ausgerichtet ist. Gefördert
wird BAKGame durch das BMWK. Die Idee hinter BAKGame ist es, IT-Awareness
und eine Sensibilisierung für Bedrohungen mittels Gamification zu vermitteln. In diesem Repository befindet sich
der Sourcecode unserer Spiele. Weitere Informationen über BAKGame,
sowie eine Onlineversion der Spiele finden Sie auf unserer Webseite
https://bakgame.de.

Der Sourcecode in diesem Repository ist freie Software unter der **AGPLv3** Lizenz.
Für weitere Informationen bezüglich der Lizenz beachten Sie bitte die `LICENSE.md`.
Wenn Sie zum Projekt beitragen möchten, zögern Sie bitte nicht Tickets zu eröffnen
oder direkt einen Merge-Request mit Ihren Änderungen zu stellen.

Sollten Sie Fragen oder Feedback haben, wenden Sie sich bitte jederzeit per E-Mail
an uns. Die E-Mail-Adresse hierfür lautet: info@bakgame.de

