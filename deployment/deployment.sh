API_TOKEN=""
WWWROOT="/usr/home/bakman/public_html"
LOCAL_PATH="/usr/home/bakman/deployment"

curl --location --header "PRIVATE-TOKEN:$API_TOKEN" "https://gitlab.com/api/v4/projects/iot-aalen%2Fbakgame/jobs/artifacts/master/download?job=deploy-build-job" --output "$LOCAL_PATH/artifacts.zip"
md5new=$(md5sum "$LOCAL_PATH/artifacts.zip" | sed 's/ .*//g')
md5old=$(md5sum "$LOCAL_PATH/artifacts.old" | sed 's/ .*//g')

if [ "$md5new" != "$md5old" ]; then
    rm -rf "$LOCAL_PATH/unzipfolder"
    mkdir "$LOCAL_PATH/unzipfolder"
    unzip "$LOCAL_PATH/artifacts.zip" -d "$LOCAL_PATH/unzipfolder"
    rm -rf "$WWWROOT/"*
    cp -r "$LOCAL_PATH/unzipfolder/code/react_frontend/build/"* "$WWWROOT"
    mv "$LOCAL_PATH/artifacts.zip" "$LOCAL_PATH/artifacts.old"
fi
