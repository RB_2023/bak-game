import React from 'react';
import {
  Route, BrowserRouter, Routes,
} from 'react-router-dom';

import SecurityCards from 'src/components/SecurityCards/SecurityCards';
import Home from 'src/components/Home/Home';

/** MainRouter component contains top level links to each game */
function MainRouter() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home/>} />
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Route path='/SecurityCards' element={<SecurityCards/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default MainRouter;
