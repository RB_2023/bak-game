import IncomeEffect from 'src/components/SecurityCards/logic/effects/IncomeEffect';
import {ICardEffect, IEffect, IIncomeEffect, IProbabilityEffect} from './EffectsApi';

/** Return an list of mocked effects **/
export function getMocupEffects(): IEffect[] {
  return [
    {
        "name": "test50-1",
        "duration": -1,
        "probability":50,
        "tag": "Test"
      } as IProbabilityEffect,
    {
        "name": "test1002",
        "duration": 2,
        "probability":100,
        "tag": "Test"
      } as IProbabilityEffect,
      {
        "name": "baz set",
        "duration": 0,
        "skillName":"baz set"
    } as ICardEffect,
    {
      "name": "testIncome10",
      "duration": -1,
      "amount": 10,
    } as IIncomeEffect,
    {
      "name": "testNegative",
      "duration": 2,
      "probability":-15,
      "tag": "Test"
    } as IProbabilityEffect
  ]
}
