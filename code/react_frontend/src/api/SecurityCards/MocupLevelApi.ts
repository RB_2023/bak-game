import {ILevel} from "./LevelsApi"

/** Return an list of mocked levels **/
export function getMocupLevels(): ILevel[] {
  return [
    {
        name: 'mocupLevel1',
        title: 'Initiales Level',
        subTitle: '',
        rounds: 10,
        description: ['foo'],
        incidentEventNames: ['foo'],
        damagingEventNames: ['bar'],
        harmlessEventNames: ['box'],
    }
  ]
}
