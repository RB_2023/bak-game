import {ISkill} from './SecuritySkillsApi';
import {Tag} from './SecurityEventsApi';

export function getMocupSkills(): ISkill[] {
  return [
    {
      playPrice: 0,
      cardType:'',
      label: 'foo',
      name: 'foo',
      description: 'foo description',
      effectText: 'foo effect',
      price: 10,
      tags: ['Test', 'Data'],
      effectValue: 50,
      isProbability: true,
      isAutoPlay:false,
      effectNames:['test50-1']
    },
    {
      playPrice: 0,
      cardType:'',
      label: 'bar',
      name: 'bar',
      description: 'bar description',
      effectText: 'bar effect',
      price: 55,
      tags: ['Test', 'Data'],
      effectValue: 100,
      isProbability: true,
      isAutoPlay:false,
      effectNames:['test1002']
    },
    {
      playPrice: 0,
      effectNames:[],
      cardType:'',
      label: 'baz set',
      name: 'baz set',
      description: 'baz set description',
      effectText: 'baz set effect',
      price: 55,
      tags: ['Test', 'Data'],
      effectValue: 100,
      isProbability: false,
      isAutoPlay:false,
      operation: "SET"
    },
    {
      playPrice: 0,
      effectNames:[],
      cardType:'',
      label: 'baz sub',
      name: 'baz sub',
      description: 'baz sub description',
      effectText: 'baz sub effect',
      price: 55,
      tags: ['Spam'],
      effectValue: 100,
      isProbability: false,
      isAutoPlay:false,
      operation: "SUB"
    },
  ]
}
