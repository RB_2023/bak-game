import {IEvent, Tag} from './SecurityEventsApi';

/** Return an list of two mocked events **/
export function getMocupEvents(): IEvent[] {
  return [
    {
      label: 'foo',
      name: 'foo',
      description: 'foo furchtbares foo',
      occurText: 'zahle 300 foo',
      notOccurText: 'badshit',
      probability: 100,
      cost: 300,
      tags: ['Test', 'Data'],
    },
    {
      label: 'bar',
      name: 'bar',
      description: 'bar furchtbares bar',
      occurText: 'zahle 100 bar',
      notOccurText: 'badshit',
      probability: 0,
      cost: 100,
      tags: ['Test', 'Spam'],
      requiredActions:['foo']
    },
    {
      label: 'baz',
      name: 'baz',
      description: 'baz harmless baz',
      occurText: 'get 100 baz',
      notOccurText: '',
      probability: 0,
      cost: 0,
      tags: ['Test'],
      category:'harmless'
    }
  ]
}
