import {IChallenge} from './ChallengeApi';

export default function getMocupChallenge(_path: string) : IChallenge {
  return {
    id: 'testMocup',
    title: 'titleMocup',
    description: 'descriptionMocup',
    level: 1,
    threatIds: ['mocupT'],
    available: true
  }
}
