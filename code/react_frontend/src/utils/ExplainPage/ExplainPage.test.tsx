import React from 'react';
import {screen, render, fireEvent} from '@testing-library/react';
import ExplainPage from './ExplainPage';

describe('ExplainPage', () => {
  test('should display all title and text', () => {
    const click = ()=> {};
    const element = <div>insElement</div>;
    const text = 'titleTextTest'
    render(<ExplainPage title={text} clickEvent={click}>{element}</ExplainPage>);
    expect(screen.getByText(text)).toBeInTheDocument();
    expect(screen.getByText('insElement')).toBeInTheDocument();
  });

  test('should use function', () => {
    const click = jest.fn();
    const element = <div>insElement</div>;
    const text = 'titleTextTest'
    render(<ExplainPage title={text} clickEvent={click}>{element}</ExplainPage>);
    const button = screen.getByText('OK!');
    fireEvent.click(button);
    expect(click).toBeCalled();
  });
});
