import React from 'react';
import {render, screen, fireEvent} from '@testing-library/react';
import Overlay from './Overlay';

describe('Overlay', ()=> {
  test('should render child element', ()=> {
    render(
      <div style={{width:'100%', height:'100%'}}>
       <Overlay visible={true}>TestTextUndSo</Overlay>
      </div>);
    expect(screen.getByText('TestTextUndSo')).toBeInTheDocument();
  });
});
