import React from 'react';
import {screen, render} from '@testing-library/react';
import ScoreView from './ScoreView';

test('ScoreView: Check that the Correct Score is displayed', () => {
  render(<ScoreView score={12}/>);
  const element = screen.getByText('Punktzahl: 12');
  expect(element).toBeInTheDocument();
});
