import axios from 'axios';

/** get a generic JSON file from Server and parse the json **/
export async function getJSON<T>(fileName: string): Promise<T> {
  const response = await axios.get(fileName);
  const data: T = response.data;
  return data;
}
