import React from 'react';
import {screen, render} from '@testing-library/react';
import FinView from './FinView';

describe('FinView', () => {
  test('should display the Correct Score', () => {
    render(<FinView score={13} maxScore={13}/>);
    const element = screen.getByText('Erreichte Punktzahl: 13 / 13');
    expect(element).toBeInTheDocument();
  });
  test('should display custom content if specified', () => {
    const custom = (<div>CustomFooBar</div>);
    render(<FinView score={13} customContent={custom} maxScore={13}/>);
    const element = screen.getByText('CustomFooBar');
    expect(element).toBeInTheDocument();
  });
});
