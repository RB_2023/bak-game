/** formats number to German currency string without fraction */
export function formatMoney(amount:number):string {
  const currency = new Intl.NumberFormat('de',
      {style: 'currency', currency: 'EUR',
        minimumFractionDigits: 0});
  return currency.format(amount);
}
