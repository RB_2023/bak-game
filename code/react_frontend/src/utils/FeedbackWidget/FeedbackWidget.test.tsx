import React from 'react';
import FeedbackWidget from './FeedbackWidget';
import {render, screen, fireEvent} from '@testing-library/react';

describe('FeedbackWidget', ()=> {
  test('should render the widget', ()=> {
    render(<FeedbackWidget position={[10,10]} link={'testlink'}/>);
    expect(screen.getByText('Geben Sie uns Feedback')).toBeInTheDocument();
  })

  test('should open window for link', ()=> {
    const testLink = 'testlink'
    render(<FeedbackWidget position={[10,10]} link={testLink}/>);
    const button = screen.getByText('Geben Sie uns Feedback');
    const spyFn = jest.spyOn(window, 'open');
    spyFn.mockImplementation((
      _url?: string | undefined | URL,
      _target?: string | undefined,
      _features?: string | undefined,
      _replace?: boolean | undefined,
      ) : Window | null => {return null})
    fireEvent.click(button);
    expect(spyFn).toHaveBeenCalledWith(testLink);
  })
});
