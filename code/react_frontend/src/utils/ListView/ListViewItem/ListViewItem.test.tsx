import React from 'react';
import {screen, render, fireEvent} from '@testing-library/react';
import ListViewItem from './ListViewItem';

class TestElement {
  name = 'TestElement';
}


describe('ListView', () => {
  describe('ListViewItem', () => {
    test('should display JSX Element', () => {
      const callback = jest.fn();
      const element = new TestElement();
      const content = (<div>TestContent</div>);
      render(<ListViewItem<TestElement> element={element}
        onClick={callback} isActive={false} content={content}/>);
      expect(screen.getByText('TestContent')).toBeInTheDocument();
    });

    test('should use the elment for callback function onClick', () => {
      const callback = jest.fn();
      const element = new TestElement();
      const content = (<div>TestContent</div>);
      render(<ListViewItem<TestElement> element={element}
        onClick={callback} isActive={false} content={content}/>);
      fireEvent.click(screen.getByRole('listitem'));
      expect(callback).toHaveBeenCalledWith(element);
    });

    test('should highlight on select', () => {
      const callback = jest.fn();
      const element = new TestElement();
      const content = (<div>TestContent</div>);
      render(<ListViewItem<TestElement> element={element}
        onClick={callback} isActive={true} content={content}/>);
      const item = screen.getByRole('listitem');
      expect(item.className).toMatch(/.*ActiveListItem.*/);
    });
  });
});
