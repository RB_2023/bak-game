import React from 'react';
import {screen, render, fireEvent} from '@testing-library/react';
import ListView from './ListView';

class TestElement {
  name: string = 'TestElement';
}


describe('ListView', () => {
  describe('ListViewItem', () => {
    test('should call display function for every Element', () => {
      const callback = jest.fn();
      const display = jest.fn();
      const element1 = new TestElement();
      let element2 = new TestElement();
      element2.name = 'TestElement2';
      render(<ListView<TestElement> renderEntry={display}
        onClick={callback} list={[element1, element2]}/>);
      expect(display).toHaveBeenCalledWith(element1, false);
      expect(display).toHaveBeenCalledWith(element2, false);
      expect(display).toHaveBeenCalledTimes(2);
    });

    test('should use the elment callback for onClick', () => {
      const callback = jest.fn();
      const display = jest.fn();
      const element = new TestElement();
      render(<ListView<TestElement> renderEntry={display}
        onClick={callback} list={[element]}/>);
      fireEvent.click(screen.getByRole('listitem'));
      expect(callback).toHaveBeenCalledWith(element);
    });

    test('should update the active element onClick', () => {
      const callback = jest.fn();
      const display = jest.fn();
      const element = new TestElement();
      const spyState = jest.spyOn(ListView.prototype, 'setState');
      render(<ListView<TestElement> renderEntry={display}
        onClick={callback} list={[element]}/>);
      fireEvent.click(screen.getByRole('listitem'));
      expect(spyState).toHaveBeenCalledWith({activeElement: element});
    });
  });
});
