import React from 'react';
import './InfoPage.scss';
import logo from 'src/assets/logo.svg';

interface InfoPageProps {
  children?: React.ReactNode[],
}

/** Generic Info Page for info landing sides etc.
 * Uses two child elements as props.
 * First is the head bar content,
 * second is the page content
 * **/
class InfoPage extends React.Component<InfoPageProps> {
  /** Default Constructor **/
  constructor(props: InfoPageProps) {
    super(props);
  }

  /** Render Funktion **/
  render() {
    let head: React.ReactNode;
    let body: React.ReactNode;

    if (this.props.children) {
      head = this.props.children[0];
      body = this.props.children[1];
    } else {
      head = <div></div>;
      body = <div></div>;
    }
    return (
      <div className="InfoPage">
        <div className="InfoPageHeader">
          <a href="https://bakgame.de">
            <img className="Logo" alt="BAK Logo"
              src={logo}
              width="28" height="28"/>
          </a>
          {head}
        </div>
        <div className="InfoPageContent">
          {body}
        </div>
      </div>
    );
  }
}

export default InfoPage;
