import React from 'react';
import {Link} from 'react-router-dom';
import './Home.scss';

/** Home component with link elements*/
function Home() {
  return (
    <div className="Home">
      <ul>
        <li><Link to="/SecurityCards">SecurityCards</Link></li>
      </ul>
    </div>
  );
}

export default Home;
