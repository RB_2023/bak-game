import {getMocupEffects} from 'src/api/SecurityCards/MocupEffectsApi';
import {screen, render} from '@testing-library/react';
import EffectViewContainer from './EffectViewContainer'
import {IProbabilityEffect} from 'src/api/SecurityCards/EffectsApi';

function checkDuration(duration: number) : string {
  if (duration > 0) {
    return '∞';
  }
  return '';
}


describe('SecurityCards', () => {
  describe('EffectViewContainer', () => {
    test('should display each effect view', () => {
      const mocupEffects = getMocupEffects().slice(0,1);
      render(<EffectViewContainer effects={mocupEffects}/>);
      const mocupProbabiltyEffect = mocupEffects[0] as IProbabilityEffect;
      const probabilityMessage = `${mocupProbabiltyEffect.probability > 0 ? 'erhöht': 'senkt'}`;
      expect(screen.getByText(probabilityMessage, {exact: false})).toBeInTheDocument();
      expect(screen.getByText(mocupProbabiltyEffect.probability, {exact: false})).toBeInTheDocument();
      expect(screen.getByText(mocupProbabiltyEffect.tag, {exact: false})).toBeInTheDocument();
      expect(screen.getAllByText(checkDuration(mocupProbabiltyEffect.duration))[0]).toBeInTheDocument();
    });
  })
})
