import {render, screen} from '@testing-library/react';
import HistoryView from './HistoryView';
import History, {Turn} from '../logic/History';
import {getMocupSkills} from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import {getMocupEvents} from 'src/api/SecurityCards/MocupSecurityEventsApi';
import Skill from '../logic/cards/Skill';
import Event from '../logic/cards/Event';


describe('SecurityCards', () => {
  describe('HistoryView', () => {
    test('should display history on end screen', () => {
      const history = new History();
      const mocedSkills = getMocupSkills();
      const event = new Event(getMocupEvents()[0]);
      const skills: Skill[] = [];
      mocedSkills.forEach(skill => skills.push(new Skill(skill)));
      const turn = {
        'event': event,
        'usedSkills': skills,
        'money': 1000
      };
      history.history.push(turn);
      render(<HistoryView history={history} />);
      expect(screen.getByText(history.history[0].event.description)).toBeInTheDocument();
      for(let i = 0; i <= history.history[0].usedSkills.length-1; i++) {
        expect(screen.getByText(history.history[0].usedSkills[i].description)).toBeInTheDocument();
      }
    });
  });
})
