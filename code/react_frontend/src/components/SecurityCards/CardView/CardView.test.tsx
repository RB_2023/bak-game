import React from 'react';
import CardView from './CardView';
import Skill from '../logic/cards/Skill';
import {getMocupSkills} from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import {TagIcons} from 'src/api/SecurityCards/SecurityEventsApi';
import {screen, render} from '@testing-library/react';

const skill: Skill = new Skill(getMocupSkills()[0]);


describe('SecurityCards', () => {
  describe('CardView', () => {
    test('should display card content', () => {
      const ref: React.RefObject<CardView> = React.createRef();

      render(<CardView ref={ref} skill={skill}
        usable={true} isMinified={false} onClickFunction={()=>null}
        />);
      expect(screen.getByText(skill.label)).toBeInTheDocument();
      expect(screen.getByText(skill.description)).toBeInTheDocument();
      const path = TagIcons[skill.tags[1]]
      if (path != undefined) {
        const tag = screen.getAllByRole('img')[1];
        expect(tag).toHaveAttribute('src', path)
      }
      expect(screen.getByText(skill.effectText)).toBeInTheDocument();
    });
    test('should display minified card content', () => {
      const ref: React.RefObject<CardView> = React.createRef();

      const { container } = render(<CardView ref={ref} skill={skill}
        usable={true} isMinified={true} onClickFunction={()=>null}
        />);
      expect(container.getElementsByClassName('CardView-Minified').length).toBe(1);
    });
    test('should display disabled card content', () => {
      const ref: React.RefObject<CardView> = React.createRef();

      const { container } = render(<CardView ref={ref} skill={skill}
        usable={false} isMinified={false} onClickFunction={()=>null}
        />);
      expect(container.getElementsByClassName('CardDisabled').length).toBe(1);
    });
  })
})
