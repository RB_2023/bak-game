import React from 'react';
import {render, screen} from '@testing-library/react';
import EventView from './EventView';
import Event from '../logic/cards/Event';
import {getMocupEvents} from 'src/api/SecurityCards/MocupSecurityEventsApi';


describe('SecurityCards', () => {
  describe('EventView', () => {
    test('should display event name and description', () => {
      const event = new Event(getMocupEvents()[0]);
      render(<EventView event={event} showEventDescription={true} />);
      expect(screen.getByText(event.name)).toBeInTheDocument();
      expect(screen.getByText(event.description)).toBeInTheDocument();
    });

    test('should display event occurs text, when occurs', () => {
      const event = new Event(getMocupEvents()[0]);
      event.occurs = true;
      render(<EventView event={event} showEventDescription={false} />);
      expect(screen.getByText(event.occurText)).toBeVisible();
    });

    test('should display occurs not when off', () => {
      const event = new Event(getMocupEvents()[0]);
      event.occurs = false;
      render(<EventView event={event} showEventDescription={false} />);
      expect(screen.getByText(event.notOccurText)).toBeVisible();
    });
  });
})
