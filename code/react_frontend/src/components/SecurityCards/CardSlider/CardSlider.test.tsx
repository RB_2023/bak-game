import React from 'react';
import {render, screen, fireEvent, act} from '@testing-library/react';
import CardSlider from './CardSlider';
import GameState from '../logic/GameState';
import Skill from '../logic/cards/Skill';
import { getMocupSkills } from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import GameData from '../logic/GameData';

function empty(){};

function mockGetSkill(_name: string): Skill {
    return new Skill(getMocupSkills()[0]);
  }

const spyOnGetSkill = jest.spyOn(GameData.prototype, 'getSkill');
describe('SecurityCards',()=>{
    beforeEach(()=>{
        spyOnGetSkill.mockImplementation(mockGetSkill);
    })
    describe('Cardslider',()=>{
        test('should only display buttons if more slides than visible slides are present',async ()=>{
            const gameState = new GameState();
            render(<CardSlider gameState={gameState} handleCardSelect={empty}></CardSlider>)
            expect(screen.queryByText('<')).not.toBeInTheDocument();
            // 6 elements
            act(()=> {
              gameState.skills.addSkillCards(['FooBarCard','FooBarCard','FooBarCard','FooBarCard','FooBarCard','FooBarCard']);
            });
            expect(await screen.findByText('<')).toBeInTheDocument();
        })
    });
});
