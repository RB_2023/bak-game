import React from 'react';
import {render, screen, fireEvent} from '@testing-library/react';
import ShopView from './ShopView';
import GameState from '../logic/GameState';
import {getMocupSkills} from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import Skill from '../logic/cards/Skill';
import GameShop from '../logic/ShopState';

function mockGetSkill(_name: string): Skill {
  return new Skill(getMocupSkills()[0]);
}

describe('SecurityCards', ()=> {
  describe('ShopView', ()=> {

    test('should display cards from GameState', ()=> {
      const state = new GameState();
      state.shop = new GameShop();
      state.shop.shopVisible = true;
      const skillCard = mockGetSkill('test');
      state.shop.cardsInShop.push(skillCard);
      render(<ShopView gameState={state}/>);
      expect(screen.getByText(skillCard.name)).toBeInTheDocument();
    })

    test('should show confirmation on card select', ()=> {
      const state = new GameState();
      state.shop = new GameShop();
      state.shop.shopVisible = true;
      const skillCard = mockGetSkill('test');
      state.shop.cardsInShop.push(skillCard);
      render(<ShopView gameState={state}/>)
      const cardElement = screen.getByText(skillCard.name);
      expect(screen.getByText('Karten Shop')).toBeInTheDocument();
      fireEvent.click(cardElement);
      expect(screen.getByText('Kaufen')).toBeInTheDocument();
    })

    test('should charge on buy', ()=> {
      const state = new GameState();
      state.shop = new GameShop();
      state.shop.shopVisible = true;
      const skillCard = mockGetSkill('test');
      state.shop.cardsInShop.push(skillCard);
      state.money = 10000;
      render(<ShopView gameState={state}/>);
      const cardElement = screen.getByText(skillCard.name);
      fireEvent.click(cardElement);
      const moneyBefore = state.money;
      fireEvent.click(screen.getByText('Kaufen'));
      expect(moneyBefore - skillCard.price).toEqual(state.money);
    });

    test('should show error on buy too expensive card', ()=> {
      const state = new GameState();
      state.shop = new GameShop();
      state.shop.shopVisible = true;
      const skillCard = mockGetSkill('test');
      state.shop.cardsInShop.push(skillCard);
      state.money = 9;
      render(<ShopView gameState={state}/>);
      const cardElement = screen.getByText(skillCard.name);
      fireEvent.click(cardElement);
      const moneyBefore = state.money;
      fireEvent.click(screen.getByText('Kaufen'));
      expect(moneyBefore).toEqual(state.money);
    })
  })
})
