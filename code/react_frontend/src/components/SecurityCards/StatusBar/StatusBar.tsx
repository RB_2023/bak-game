import React from 'react';
import {observer} from 'mobx-react';
import './StatusBar.scss';
import GameState from 'src/components/SecurityCards/logic/GameState';
import damaging from 'src/assets/SecurityCards/eventcard_damaging.svg';
import harmless from 'src/assets/SecurityCards/eventcard_harmless.svg';
import incident from 'src/assets/SecurityCards/eventcard_incidents.svg';
import EffectViewContainer from '../EffectView/EffectViewContainer';
import {isProbabilityEffect} from '../logic/effects/ProbabilityEffect';

interface StatsViewProps {
  gameState: GameState
}

interface StatusBarState {
  visibleMoney: boolean
}

/**
 * Class for the Status bar, displaying
 * left side elements.
 */
@observer
class StatusBar extends React.Component<StatsViewProps, StatusBarState> {
  /** default constructor */
  constructor(props: StatsViewProps) {
    super(props);
    this.state = {
      visibleMoney: false,
    };
  }

  /** Toggle money visibility **/
  toggleMoneyVisibility = ()=> {
    this.setState({visibleMoney: !this.state.visibleMoney});
  }

  /** handle button click.
   * either go to next turn when event description is not toggled or
   * check if current active event occurs in the game state.
   * If so, add the effect and toggle the event description.
  **/
  buttonClick = () => {
    if (!this.props.gameState.showEventDescription) {
      this.props.gameState.nextTurn();
      this.props.gameState.toggleEventDescription();
    } else {
      this.props.gameState.checkEvent();
      this.props.gameState.toggleProbMeterAnimation();
    }
  }

  /** Render the StatView and its elements **/
  render() {
    let cardImg;
    switch (this.props.gameState.activeEvent?.category) {
      case 'harmless':
        cardImg = harmless;
        break;
      case 'damaging':
        cardImg = damaging;
        break;
      case 'incident':
        cardImg = incident;
        break;
    }
    return (
      <div className='StatusBar'>
        <button
          className="NextButton"
          disabled={(!this.props.gameState.nextTurnEnabled() &&
             !this.props.gameState.showEventDescription )||
             this.props.gameState.probMeterIsMoving}
          title={
            (!this.props.gameState.nextTurnEnabled() &&
            !this.props.gameState.showEventDescription)?
            'Aktion erforderlich!':''}
          onClick={() => this.buttonClick()}>
          {this.props.gameState.showEventDescription ? 'OK' : 'Nächste Woche'}
        </button>

        <div className='ImageFrame'>
          <img className='EventIcon' src={cardImg}></img>
        </div>

        <EffectViewContainer
          effects={this.props.gameState.activeEffects.effects.filter(
              (effect) => isProbabilityEffect(effect))}/>
        <button
          className="ShopButton"
          disabled={!this.props.gameState.shopAvailable}
          onClick={()=>this.props.gameState.shop.toggleShopVisibility()}
        >Kartenshop</button>
      </div>
    );
  }
}

export default StatusBar;
