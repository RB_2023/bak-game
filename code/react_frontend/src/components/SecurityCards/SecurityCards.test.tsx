import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import SecurityCards from './SecurityCards';
import GameState from './logic/GameState';
import {Page} from './logic/GameState';
import GameData from './logic/GameData';
import { getMocupSkills } from 'src/api/SecurityCards/MocupSecuritySkillsApi'
import { getMocupEvents } from 'src/api/SecurityCards/MocupSecurityEventsApi'
import { getMocupEffects } from 'src/api/SecurityCards/MocupEffectsApi';
import { getMocupLevels } from 'src/api/SecurityCards/MocupLevelApi';
import Event from './logic/cards/Event';
import IncomeEffect from './logic/effects/IncomeEffect';
import { IIncomeEffect } from 'src/api/SecurityCards/EffectsApi';
import * as levelAPI from 'src/api/SecurityCards/LevelsApi';
import Skill from './logic/cards/Skill';

const getSkillSpy = jest.spyOn(GameData.prototype, 'getSkill');
const getEventSpy = jest.spyOn(GameData.prototype, 'getEvent');
const getEffectSpy = jest.spyOn(GameData.prototype, 'getEffect');
const loadSpy = jest.spyOn(levelAPI, 'getLevels');

const flushPromises = () => new Promise(resolve => setTimeout(resolve, 0));

describe('SecurityCards', () => {
  beforeEach(() => {
    getSkillSpy.mockImplementation((_: string) => {
      return new Skill(getMocupSkills()[0]);
    });
    getEventSpy.mockImplementation((_: string) => {
      return new Event(getMocupEvents()[1]);
    });
    getEffectSpy.mockImplementation((_) => {
      return new IncomeEffect(getMocupEffects()[3] as IIncomeEffect)
    })
    loadSpy.mockImplementation(async () => {
      return getMocupLevels()
    });
  })

  test('should create new GameState on create', () => {
    const ref: React.RefObject<SecurityCards> = React.createRef();
    render(<SecurityCards ref={ref} />);
    expect(ref.current?.gameState).toBeInstanceOf(GameState);
  })

  test('should begin with explain page', async () => {
    const ref: React.RefObject<SecurityCards> = React.createRef();
    render(<SecurityCards ref={ref} />);
    await flushPromises();
    expect(screen.getByText('SecurityCards Anleitung'));
  })

  test('should load levels async on mount', async () => {
    const ref: React.RefObject<SecurityCards> = React.createRef();
    render(<SecurityCards ref={ref} />);
    if (ref.current?.gameState.data.levels !== undefined) {
      ref.current.gameState.data.levels = getMocupLevels();
    }
    await flushPromises();
    fireEvent.click(screen.getByText('OK!'));
    await flushPromises();
    expect(screen.getAllByText(getMocupLevels()[0].title)[0]).toBeInTheDocument();
  })

  test('should disable button on blocking event', async () => {
    const ref: React.RefObject<SecurityCards> = React.createRef();
    render(<SecurityCards ref={ref} />);
    if (ref.current?.gameState.data.levels !== undefined) {
      ref.current.gameState.data.levels = getMocupLevels();
    }
    await flushPromises();
    fireEvent.click(screen.getByText('OK!'));
    await flushPromises();
    const eventButton = screen.getByText('Initiales Level');
    expect(eventButton).toBeInTheDocument();
    fireEvent.click(eventButton);
    let event;
    if (ref.current?.gameState.activeEvent?.occurs) {
    event = ref.current?.gameState.activeEvent;
    event.occurs = true;
    expect(screen.getByText('Nächste Woche')).toBeInTheDocument();
    expect(screen.getByText('Nächste Woche')).toBeDisabled();
    }
  })
})
