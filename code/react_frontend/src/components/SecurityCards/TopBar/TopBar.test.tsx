import React from 'react';
import {screen, render} from '@testing-library/react';
import TopBar from './TopBar';
import GameState from '../logic/GameState';
import GameData from '../logic/GameData';
import {getMocupLevels} from 'src/api/SecurityCards/MocupLevelApi';

describe('SecurityCards TopBar', () => {
  test('Check that the correct stats values are displayed', async () => {
    const gameState = new GameState;
    gameState.money = 100;
    gameState.turn = 1;
    gameState.data = new GameData;
    gameState.data.loadedLevel = getMocupLevels()[0];
    const rounds = gameState.getMaxRounds();

    render(<TopBar gameState={gameState}/>);
      await new Promise((r) => setTimeout(r, 2000));
    const money = await screen.findByText(`Geld: ${gameState.money} €`);
    expect(money).toBeInTheDocument();
    const turn = screen.getByText(
      `KW ${gameState.turn} von ${rounds}`);
    expect(turn).toBeInTheDocument();
  });
})
