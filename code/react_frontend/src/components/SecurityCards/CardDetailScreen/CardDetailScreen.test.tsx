import React from 'react';
import CardDetailScreen from './CardDetailScreen';
import Skill from '../logic/cards/Skill';
import {getMocupSkills} from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import {getMocupEvents} from 'src/api/SecurityCards/MocupSecurityEventsApi';
import Event from '../logic/cards/Event';
import {screen, render, act} from '@testing-library/react';


function useFunction(skill: Skill) {
  /** Do nothing **/
}

const skill: Skill = new Skill(getMocupSkills()[0]);

describe('SecurityCards', () => {
  describe('CardDetailScreen', () => {
    test('should not be visible on default', () => {
      const ref: React.RefObject<CardDetailScreen> = React.createRef();
      const mockupEvents = getMocupEvents()

      render(<CardDetailScreen ref={ref} useFunction={useFunction} 
        activeEvent={new Event(mockupEvents[0])}
        />);
      expect(ref.current?.state.visible).toEqual(false);
    });

    test('should be visible after show', () => {
      const ref: React.RefObject<CardDetailScreen> = React.createRef();
      const mockupEvents = getMocupEvents()

      render(<CardDetailScreen ref={ref} useFunction={useFunction} 
        activeEvent={new Event(mockupEvents[0])}
        />);
      act(() => {
        ref.current?.show(skill);
      });
      expect(ref.current?.state.visible).toEqual(true);
    });

    test('should hide ater hide', () => {
      const ref: React.RefObject<CardDetailScreen> = React.createRef();
      const mockupEvents = getMocupEvents()

      render(<CardDetailScreen ref={ref} useFunction={useFunction} 
        activeEvent={new Event(mockupEvents[0])}
        />);
      act(() => {
        ref.current?.setState({visible: true});
      });
      expect(ref.current?.state.visible).toEqual(true);
      act(() => {
        ref.current?.hide();
      });
      expect(ref.current?.state.visible).toEqual(false);
    });

  })
})
