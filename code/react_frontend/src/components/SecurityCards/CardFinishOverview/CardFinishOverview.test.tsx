import React from 'react';
import CardFinishOverview from './CardFinishOverview';
import {screen, render} from '@testing-library/react';


describe('SecurityCards', () => {
  describe('CardFinishOverview', () => {
    test('should display winning finish page', () => {
      const ref: React.RefObject<CardFinishOverview> = React.createRef();
      const money = 1000;
      const winMessage = 'Sie haben das Unternehmen für 12 Wochen erfolgreich geleitet';
      render(<CardFinishOverview money={money}/>);
      expect(screen.getByText(winMessage)).toBeInTheDocument();
    });
    test('should display losing finish page', () => {
      const ref: React.RefObject<CardFinishOverview> = React.createRef();
      const money = -1;
      const loseMessage = 'Sie sind insolvent!';
      render(<CardFinishOverview money={money}/>);
      expect(screen.getByText(loseMessage)).toBeInTheDocument();
    });
  })
})
