import {screen, render, fireEvent} from '@testing-library/react';
import SecurityCardsLevelSelection from './SecurityCardsLevelSelection';
import {getMocupLevels} from 'src/api/SecurityCards/MocupLevelApi';

describe('SecurityCardsExplainPage', () => {
  test('should use clickEvent', () => {
    const click = jest.fn();
    const testLevel = getMocupLevels();
    render(<SecurityCardsLevelSelection clickEvent={click}
      levels={testLevel}/>);
    expect(click).toBeCalledTimes(0);
    expect(screen.getByText(testLevel[0].title)).toBeInTheDocument();
    fireEvent.click(screen.getByText(testLevel[0].title));
    expect(click).toBeCalled();
  });

  test('should render levels', () => {
    const testLevels = getMocupLevels();
    const click = jest.fn();
    render(<SecurityCardsLevelSelection clickEvent={click}
      levels={testLevels}/>);
    expect(screen.getByText(testLevels[0].title)).toBeInTheDocument();
  });
});
