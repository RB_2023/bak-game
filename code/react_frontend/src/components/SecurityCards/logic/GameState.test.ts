import GameState from './GameState';
import GameData from './GameData';

import Skill from './cards/Skill';
import Event from './cards/Event';


import {getMocupSkills} from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import {getMocupEvents} from 'src/api/SecurityCards/MocupSecurityEventsApi';
import { getMocupEffects } from 'src/api/SecurityCards/MocupEffectsApi';
import IncomeEffect from './effects/IncomeEffect';
import { IIncomeEffect } from 'src/api/SecurityCards/EffectsApi';
import Effect from './effects/Effect';
import { IEvent } from 'src/api/SecurityCards/SecurityEventsApi';
import {getMocupLevels} from 'src/api/SecurityCards/MocupLevelApi';

function mockGetSkill(_name: string): Skill {
  return new Skill(getMocupSkills()[0]);
}
function mockGetEvent(_name: string): Event {
  return new Event(getMocupEvents()[0]);
}
function mockGetEffect(_name: string): Effect {
  return new IncomeEffect(getMocupEffects()[3] as IIncomeEffect);
}


const spyOnGetSkill = jest.spyOn(GameData.prototype, 'getSkill');
const spyOnGetEvent = jest.spyOn(GameData.prototype, 'getEvent');
const spyOnGetEffect = jest.spyOn(GameData.prototype, 'getEffect');

beforeEach(()=>{
  spyOnGetSkill.mockImplementation(mockGetSkill);
  spyOnGetEvent.mockImplementation(mockGetEvent);
  spyOnGetEffect.mockImplementation(mockGetEffect);
})

describe('SecurityCards', () => {
  describe('GameState', () => {
    test('should get skill cards from data on addSkillCards', () => {
      const gameState = new GameState();
      gameState.skills.addSkillCards(['FooBarCard']);
      expect(spyOnGetSkill).toBeCalledWith('FooBarCard');
    });

    test('should return next event if one exists', () => {
      const gameState = new GameState();
      gameState.skills.addSkillCards(gameState.initialSkills);
      gameState.upcomingEvents = ['foo','foo'];
      const event = gameState.nextEvent();
      expect(event).toEqual(mockGetEvent('foo'));
    });

    test('should return undefined if no next event', () => {
      const gameState = new GameState();
      gameState.initialEffects = ['testIncome10'];
      gameState.init();
      
      gameState.upcomingEvents = [];
      expect(gameState.nextEvent()).toBeUndefined();
    });

    test('should change money by incomeValues', ()=> {
      const gameState = new GameState();
      const incomeEffect = new IncomeEffect(getMocupEffects()[3] as IIncomeEffect);
      gameState.upcomingEvents = ['foo','foo'];
      gameState.activeEffects.addEffect(incomeEffect);
      gameState.nextTurn();
      expect(gameState.money).toEqual(incomeEffect.amount);
    });

    test('should load level on level selection', () => {
      const gameState = new GameState();
      const loadLevelSpy = jest.spyOn(gameState.data, 'loadLevel');
      gameState.data.levels = getMocupLevels();
      gameState.selectLevel(gameState.data.levels[0]);
      expect(loadLevelSpy).toHaveBeenCalled();
    });

    test('should remove skill on removeSkillCard', () => {
      const gameState = new GameState();
      const skillIn = gameState.data.getSkill("test");
      gameState.skills.persistentCards.push(skillIn);
      const skillOut = gameState.skills.handCards[0];
      expect(skillOut).toBeDefined();
      gameState.skills.removeSkillCard(skillOut);
      expect(gameState.skills.handCards).not.toContain(skillOut);
    });

    test('should apply skill with SUB operation to gameState', () => {
      const gameState = new GameState();
      const decreaseMoneySpyOn = jest.spyOn(gameState, 'decreaseMoney');
      const addToUsedSkillsSpyOn = jest.spyOn(gameState.skills, 'addToUsedSkills');
      const skill = new Skill(getMocupSkills()[3]);
      const event = new Event(getMocupEvents()[0]);
      gameState.activeEvent = event;
      gameState.applySkill(skill);
      const cost = event.cost -= skill.effectValue;
      if (cost < 0) {
        expect(event.cost).toBe(0);
      } else {
        expect(event.cost).toBe(cost);
      }
      if (skill.playPrice) {
        expect(decreaseMoneySpyOn).toBeCalledWith(skill.playPrice);
      }
      expect(addToUsedSkillsSpyOn).toBeCalledWith(skill);
    });

    test('should apply skill with SET operation to gameState', () => {
      const gameState = new GameState();
      const decreaseMoneySpyOn = jest.spyOn(gameState, 'decreaseMoney');
      const addToUsedSkillsSpyOn = jest.spyOn(gameState.skills, 'addToUsedSkills');
      const skill = new Skill(getMocupSkills()[2]);
      const event = new Event(getMocupEvents()[0]);
      gameState.activeEvent = event;
      gameState.applySkill(skill);
      expect(event.cost).toEqual(skill.effectValue);
      if (skill.playPrice) {
        expect(decreaseMoneySpyOn).toBeCalledWith(skill.playPrice);
      }
      expect(addToUsedSkillsSpyOn).toBeCalledWith(skill);
    });


    test('should apply probabilty skill to gameState', () => {
      const gameState = new GameState();
      const decreaseMoneySpyOn = jest.spyOn(gameState, 'decreaseMoney');
      const addToUsedSkillsSpyOn = jest.spyOn(gameState.skills, 'addToUsedSkills');
      const skill = new Skill(getMocupSkills()[0]);
      const event = new Event(getMocupEvents()[0]);
      gameState.activeEvent = event;
      gameState.applySkill(skill);
      if (skill.playPrice) {
        expect(decreaseMoneySpyOn).toBeCalledWith(skill.playPrice);
      }
      expect(addToUsedSkillsSpyOn).toBeCalledWith(skill);
    });

    test('should apply Events to gameState', () => {
      const template: IEvent = getMocupEvents()[0];
      const event: Event = new Event(template);
      const gameState = new GameState();
      gameState.money = 500;
      gameState.activeEvent = event;
      gameState.checkEvent()
      gameState.handleEventActivation();
      expect(gameState.money).toEqual(500 - event.cost);
    }),

    test('should not apply Events with probability 0percent', () => {
      const template: IEvent = getMocupEvents()[1];
      const event: Event = new Event(template);
      const gameState = new GameState();
      gameState.activeEvent = event;
      gameState.money = 500;
      gameState.checkEvent()
      gameState.handleEventActivation()
      expect(gameState.money).toEqual(500);
    })

    test('should add negative effects to dice-roll', () => {
      jest.spyOn(global.Math, 'random').mockReturnValue(0.06);
      const template: IEvent = getMocupEvents()[0];
      template.probability = 80;
      const event: Event = new Event(template);
      const gameState = new GameState();
      gameState.activeEvent = event;
      const effects = getMocupEffects();
      gameState.activeEffects.addEffect(effects[4]);
      const occur = gameState.checkEvent();
      expect(occur).toBe(true);
    })

    test('should add positive effects to dice-roll', () => {
      jest.spyOn(global.Math, 'random').mockReturnValue(0.50);
      const template: IEvent = getMocupEvents()[0];
      template.probability = 80;
      const event: Event = new Event(template);
      const gameState = new GameState();
      gameState.activeEvent = event;
      const effects = getMocupEffects();
      gameState.activeEffects.addEffect(effects[0]);
      const occur = gameState.checkEvent();
      expect(occur).toBe(false);
    })

    test('should reduce random-value if over 100percent', () => {
      jest.spyOn(global.Math, 'random').mockReturnValue(0.99);
      const template: IEvent = getMocupEvents()[0];
      const event: Event = new Event(template);
      const gameState = new GameState();
      gameState.activeEvent = event;
      const effects = getMocupEffects();
      gameState.activeEffects.addEffect(effects[4]);
      gameState.checkEvent();
      expect(gameState.activeEvent.roll).toBe(85);
    })

  })
})
