import GameData from './GameData';
import {getMocupEvents} from 'src/api/SecurityCards/MocupSecurityEventsApi';
import {getMocupSkills} from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import {getMocupEffects} from 'src/api/SecurityCards/MocupEffectsApi';
import {getMocupLevels} from 'src/api/SecurityCards/MocupLevelApi';
import * as eventAPI from 'src/api/SecurityCards/SecurityEventsApi';
import * as skillAPI from 'src/api/SecurityCards/SecuritySkillsApi';
import * as effectAPI from 'src/api/SecurityCards/EffectsApi';
import ProbabilityEffect from './effects/ProbabilityEffect';
import Skill from '../logic/cards/Skill';
import Event from './cards/Event';

async function mockgetEvents(prefix:string, _eventNames: string[]): Promise<eventAPI.IEvent[]> {
  let rval: eventAPI.IEvent[] = [];
  const mockEvents: eventAPI.IEvent[] = getMocupEvents();
  for (let i=0; i<=mockEvents.length; i++) {
    if (mockEvents[i]) {
      rval.push(mockEvents[i])
    }
  }
  return rval;
}
async function mockgetSkills(_skillNames: string[]): Promise<skillAPI.ISkill[]>{
  let rval: skillAPI.ISkill[] = [];
  const mockSkills: skillAPI.ISkill[] = getMocupSkills();
  for (let i=0; i<=mockSkills.length; i++) {
    if (mockSkills[i]) {
      rval.push(mockSkills[i])
    }
  }
  return rval;
}

async function mockgetEffects(_skillNames: string[]): Promise<effectAPI.IEffect[]>{
  let rval: effectAPI.IEffect[] = [];
  const mockEffects: effectAPI.IEffect[] = getMocupEffects();
  for (let i=0; i<=mockEffects.length; i++) {
    if (mockEffects[i]) {
      rval.push(mockEffects[i])
    }
  }
  return rval;
}

const spyGetEvents = jest.spyOn(eventAPI, 'getEvents');
const spyGetSkills = jest.spyOn(skillAPI, 'getSkills');
const spyGetEffects = jest.spyOn(effectAPI, 'getEffects');

const flushPromises = () => new Promise(resolve => setTimeout(resolve, 0));

describe('SecurityCards/GameData', ()=> {
  beforeEach(()=> {
    spyGetEvents.mockImplementation(mockgetEvents);
    spyGetSkills.mockImplementation(mockgetSkills);
    spyGetEffects.mockImplementation(mockgetEffects);
  });

  test('should async load & map data on load', async ()=> {
    let gameData = new GameData();
    expect(gameData.events.size).toEqual(0);
    expect(gameData.skills.size).toEqual(0);
    expect(gameData.effects.size).toEqual(0);
    await gameData.loadLevel(getMocupLevels()[0]);

    let testEvents = await mockgetEvents('',['bla']);
    let testSkills = await mockgetSkills(['bla']);
    let testEffects = await mockgetEffects(['bla']);

    const testSkill = new Skill(testSkills[0]);
    const effect = new ProbabilityEffect(testEffects[0] as effectAPI.IProbabilityEffect)
    effect.skillName = testSkill.name;
    testSkill.effects =[effect];
    await flushPromises();
    testEvents[0].category = 'damaging'
    expect(gameData.getEvent('foo')).toEqual(new Event(testEvents[0]));
    expect(gameData.getSkill('foo')).toEqual(testSkill);
    expect(gameData.getEffect('test50-1')).toEqual(testEffects[0]);
  });

  test('should return correct upcomming levels', () => {
    let gameData = new GameData();
    gameData.loadedLevel = getMocupLevels()[0];
    const damaginEventName = getMocupLevels()[0].damagingEventNames[0];
    const incidentEventName = getMocupLevels()[0].incidentEventNames[0];
    let result = gameData.getUpcomingEvents();
    expect(result.length).toBeLessThanOrEqual(gameData.loadedLevel.rounds);
    let poped = result.pop();
    expect(poped).not.toEqual(damaginEventName);
    expect(poped).not.toEqual(incidentEventName);
    poped = result.pop();
    expect(poped).not.toEqual(damaginEventName);
    expect(poped).not.toEqual(incidentEventName);
  })
})
