import { ICardEffect } from 'src/api/SecurityCards/EffectsApi';
import { getMocupEffects } from 'src/api/SecurityCards/MocupEffectsApi';
import { getMocupSkills } from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import Skill from './cards/Skill';
import CardEffect from './effects/CardEffect';
import Effect from './effects/Effect';
import Effects from './effects/Effects';
import GameData from './GameData';
import SkillContainer from './SkillContainer';

function mockGetSkill(_name: string): Skill {
    return new Skill(getMocupSkills()[0]);
}
function mockGetEffect(_name: string): Effect {
    return new CardEffect(getMocupEffects()[3] as ICardEffect, mockGetSkill('foo'));
}


const spyOnGetSkill = jest.spyOn(GameData.prototype, 'getSkill');
const spyOnGetEvent = jest.spyOn(GameData.prototype, 'getEvent');
const spyOnGetEffect = jest.spyOn(GameData.prototype, 'getEffect');

beforeEach(() => {
    spyOnGetSkill.mockImplementation(mockGetSkill);
    spyOnGetEffect.mockImplementation(mockGetEffect);
})

describe('SecurityCards', () => {
    describe('SkillContainerTest', () => {
        test('should add and remove skills', () => {
            const data = new GameData;
            const effects = new Effects()
            const skillContainer = new SkillContainer(data, effects);
            const skill = mockGetSkill('foo');
            skillContainer.addSkill(skill)
            expect(skillContainer.persistentCards.length).toBe(1);
            skillContainer.removeSkillCard(skill);
            expect(skillContainer.persistentCards.length).toBe(0);
        })
        test('should add card from effect',()=>{
            const data = new GameData;
            const effects = new Effects()
            const skillContainer = new SkillContainer(data, effects);
            effects.addEffect(mockGetEffect('foo'));
            expect(skillContainer.handCards.length).toEqual(1);
        }),
        test('should add and clear used skills',()=>{
            const data = new GameData;
            const effects = new Effects()
            const skillContainer = new SkillContainer(data, effects);
            const skill = mockGetSkill('foo');
            skillContainer.addToUsedSkills(skill);
            expect(skillContainer.usedSkills.length).toBe(1);
            skillContainer.clearUsedSkills()
            expect(skillContainer.usedSkills.length).toBe(0)
        })

    })
})