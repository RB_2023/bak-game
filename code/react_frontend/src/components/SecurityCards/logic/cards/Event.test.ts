import Event from './Event';
import {IEvent} from  'src/api/SecurityCards/SecurityEventsApi';
import {getMocupEvents} from 'src/api/SecurityCards/MocupSecurityEventsApi';
import GameState from '../GameState';

describe('SecurityCardGame', ()=>{
  describe('Event', ()=>{
    test('should use IEvent attributes in the constructor', () => {
      const template: IEvent = getMocupEvents()[0];
      const event: Event = new Event(template);
      expect(event.name).toEqual(template.name);
      expect(event.description).toEqual(template.description);
      expect(event.probability).toEqual(template.probability);
      expect(event.cost).toEqual(template.cost);
      expect(event.tags).toEqual(template.tags);
    }),

    test('should apply Events to gamestate', () => {
      const template: IEvent = getMocupEvents()[0];
      const event: Event = new Event(template);
      let gamestate: GameState = new GameState();
      gamestate.money = 500;
      event.applyTo(gamestate);
      expect(gamestate.money).toEqual(500 - event.cost);
    })
  });
})
