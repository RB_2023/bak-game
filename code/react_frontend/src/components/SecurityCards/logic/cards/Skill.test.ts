import Skill from './Skill';
import {ISkill} from 'src/api/SecurityCards/SecuritySkillsApi';
import {getMocupSkills} from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import {getMocupEvents} from 'src/api/SecurityCards/MocupSecurityEventsApi';
import Event from './Event';
import GameState from '../GameState';

class TestSkill extends Skill {
  constructor(template: ISkill) {
    super(template);
  }

  applyTo(_state: GameState): boolean {
    return true
  }
}

describe('Security Cards', () => {
  describe('Events', () => {
    test('should create from interface', () => {
      const mocupSkill: ISkill = getMocupSkills()[0];
      const skill: TestSkill = new TestSkill(getMocupSkills()[0]);
      expect(skill.name).toEqual(mocupSkill.name);
      expect(skill.description).toEqual(mocupSkill.description);
      expect(skill.price).toEqual(mocupSkill.price);
      expect(skill.tags).toEqual(mocupSkill.tags);
    }),

    test('should check if skill matches the event', () => {
      const mocupSkill: ISkill = getMocupSkills()[0];
      let skill: TestSkill = new TestSkill(mocupSkill);
      const event1: Event = new Event(getMocupEvents()[0]);
      const event2: Event = new Event(getMocupEvents()[0]);
      skill.tags = ['Test', 'UserFault'];
      skill.isProbability = false;
      event1.tags = ['Test', 'Data'];
      event2.tags = ['Data', 'DOS'];

      expect(skill.checkFor(event1)).toBeTruthy();
      expect(skill.checkFor(event2)).toBeFalsy();
    })
  })
})
