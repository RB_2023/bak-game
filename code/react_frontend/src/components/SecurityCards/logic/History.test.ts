import { getMocupEvents } from 'src/api/SecurityCards/MocupSecurityEventsApi';
import { getMocupSkills } from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import GameState from './GameState';
import History from './History';
import Event from './cards/Event';
import Skill from './cards/Skill';
import GameData from './GameData';

function mockGetSkill(_name: string): Skill {
  return new Skill(getMocupSkills()[0]);
}
const spyOnGetSkill = jest.spyOn(GameData.prototype, 'getSkill');

beforeEach(()=>{
  spyOnGetSkill.mockImplementation(mockGetSkill);
})
describe('SecurityCards', () => {
  describe('History', () => {
    test('should add turn from current gamestate',() => {
      const history = new History;
      const gameState = new GameState;
      gameState.money = 0;
      history.addTurn(gameState);
      expect(history.history.length).toEqual(0);
      gameState.activeEvent = new Event(getMocupEvents()[0]);
      gameState.skills.usedSkills = [getMocupSkills()[0].name];
      history.addTurn(gameState);
      expect(history.history.length).toEqual(1);
      const turn = history.history[0];
      expect(turn.money).toEqual(0);
      expect(turn.event).toEqual(new Event(getMocupEvents()[0]));
    }),
    test('should create Event to skill map', ()=>{
      const history = new History
      const gameState = new GameState;
      gameState.data = new GameData;
      const eventMap = new Map<Event, Skill[]>();
      //harmless event case
      gameState.activeEvent = new Event(getMocupEvents()[2]);
      history.addTurn(gameState);
      //harmful event with matching skills
      gameState.activeEvent = new Event(getMocupEvents()[0]);
      history.addTurn(gameState);
      eventMap.set(new Event(getMocupEvents()[0]),
        [new Skill(getMocupSkills()[0])]);
      gameState.shop.shopableSkills=[getMocupSkills()[0].name];
      history.createEventMap(gameState);
      expect(history.eventMap).toEqual(eventMap);
    })
  })
})