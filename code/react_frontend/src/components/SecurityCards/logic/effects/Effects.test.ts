import Effects from './Effects';
import {getMocupEffects} from 'src/api/SecurityCards/MocupEffectsApi';
import ProbabilityEffect from './ProbabilityEffect';
import { ICardEffect, IIncomeEffect, IProbabilityEffect } from 'src/api/SecurityCards/EffectsApi';
import CardEffect from './CardEffect';
import { getMocupSkills } from 'src/api/SecurityCards/MocupSecuritySkillsApi';
import IncomeEffect from './IncomeEffect';
import Skill from '../cards/Skill';

describe('SecurityCards/Effects', ()=> {
  test('should decrease duration of awarness items on tick', () => {
    const effects = new Effects();
    const templates = getMocupEffects();
    const effect = new ProbabilityEffect(templates[1]as IProbabilityEffect);
    effects.addEffect(effect);
    effects.tick();
    expect(effects.effects[0].duration).toEqual(1);
  });

  test('should return correct awareness sum up', () => {
    const effects = new Effects();
    const templates = getMocupEffects();
    const effect1 = new ProbabilityEffect(templates[0]as IProbabilityEffect);
    const effect2 = new ProbabilityEffect(templates[1]as IProbabilityEffect);
    effects.addEffect(effect1)
    effects.addEffect(effect2);
    expect(effects.awarenessLevels['Test']).toEqual(100);
  });

  test('should add CardEffect\'s skill to effectSkills',()=>{
    const effects = new Effects();
    const templates = getMocupEffects();
    const skills = getMocupSkills();
    const skill = new Skill(skills[2]);
    const cardEffect = new CardEffect(templates[2] as ICardEffect,skill);
    effects.addEffect(cardEffect);
    expect(effects.effectSkills).toContain(skill);
  });
  test('should remove cardEffect on removeCardEffect',()=>{
    const effects = new Effects();
    const templates = getMocupEffects();
    const skills = getMocupSkills();
    const skill = new Skill(skills[2]);
    const cardEffect = new CardEffect(templates[2] as ICardEffect,skill);
    effects.addEffect(cardEffect);
    expect(effects.effects).toContain(cardEffect);
    effects.removeCardEffect(skill);
    expect(effects.effects).not.toContain(cardEffect);
  });
  test('should correctly compute incommeValues',()=>{
    const effects = new Effects();
    const templates = getMocupEffects();
    const effect = new IncomeEffect(templates[3] as IIncomeEffect);
    effects.addEffect(effect);
    expect(effects.incomeValues).toEqual([10]);
  });
})
