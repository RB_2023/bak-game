import Event from './cards/Event';
import {makeObservable, observable, action, computed} from 'mobx';
import GameData from './GameData';
import Effects from './effects/Effects';
import ShopState from './ShopState';
import SkillContainer from './SkillContainer';
import History from './History';
import Skill from './cards/Skill';
import {ILevel} from 'src/api/SecurityCards/LevelsApi';
import ProbabilityEffect from './effects/ProbabilityEffect';

export enum Page {
  loading,
  explain,
  game,
  finished,
  levelselection,
}

export const MAX_ROUNDS = 12;
/** gamestate class with state variables and
 * functionality for SecurityCards component */
export default class GameState {
  activeEvent: Event | undefined;
  money = 0;
  eventsInLevel = 0;
  turn = 0;
  data: GameData = new GameData;
  history = new History()
  page: Page = Page.explain;
  shop: ShopState = new ShopState();
  activeEffects: Effects = new Effects();
  skills: SkillContainer = new SkillContainer(this.data, this.activeEffects);
  showEventDescription = true;
  probMeterIsMoving = false;
  // Initial data
  upcomingEvents: string[] = [];
  initialSkills = ['CreateBackups'];

  initialEffects = ['incomeBasicIncome'];
  // Level Events
  // Variable Kalenderwoche
  /** Constructor setting up MobX **/
  constructor() {
    makeObservable(this, {
      nextTurnEnabled: action,
      activeEvent: observable,
      money: observable,
      turn: observable,
      page: observable,
      showEventDescription: observable,
      probMeterIsMoving: observable,
      decreaseMoney: action,
      increaseMoney: action,
      applyMoneyEffects: action,
      checkEvent: action,
      addUpcomingEvents: action,
      nextEvent: action,
      showFinishedPage: action,
      startGame: action,
      toggleEventDescription: action,
      toggleProbMeterAnimation: action,
      handleEventActivation: action,
      init: action,
      showLevelSelectionPage: action,
      showLoadingPage: action,
      shopAvailable: computed,
      getMaxRounds: action,
    });
  }

  /** set maximum available playing rounds */
  getMaxRounds() : number {
    if (this.data.loadedLevel == undefined) {
      return 0;
    }
    return this.data.loadedLevel.rounds;
  }

  /** decrease money by given value */
  decreaseMoney(value: number) {
    this.money -= value;
  }

  /** increase money by given value */
  increaseMoney(value: number) {
    this.money += value;
  }

  /** prepare LevelSelection of Events */
  selectLevel(level: ILevel) {
    this.showLoadingPage();
    this.data.loadLevel(level).then(
        () => {
          this.getMaxRounds();
          this.startGame();
        },
    );
  }

  /**
  * loads the upcomming events from game data
  */
  addUpcomingEvents() {
    this.upcomingEvents = this.data.getUpcomingEvents();
  }

  /** shop is disabled when incident appears */
  get shopAvailable(): boolean {
    return !(this.activeEvent?.category == 'incident' &&
    this.showEventDescription);
  }

  /** Check if the event occurs,
   * this depends on the awarness levels and a random
   * number. the evaluation result is saved
   **/
  checkEvent() {
    if (this.activeEvent == undefined) {
      return false;
    }
    if (this.activeEvent.occurs != undefined) {
      return this.activeEvent.occurs;
    }
    const randomValue = Math.floor((Math.random() * 100));
    /** Sum up positive and negative effects separately.
     * Random number + negative effects is capped at 100.
     * It is the minimum of either the random number
     * or 100 - negative effects.
     * The Event occurs if the random value + negative
     * effects is greater than the inverse base protection
     * + positive effects.
     **/
    let positiveEffects = 0;
    let negativeEffects = 0;
    for (const effect of this.activeEffects.effects) {
      const {tag, probability} = effect as ProbabilityEffect;
      if (this.activeEvent.tags.includes(tag) && probability < 0) {
        negativeEffects += Math.abs(probability);
      } else if (this.activeEvent.tags.includes(tag)) {
        positiveEffects += probability;
      }
    }
    const attackValue = Math.min(randomValue, 100 - negativeEffects);
    this.activeEvent.occurs = (attackValue + negativeEffects >
      100 - this.activeEvent.probability + positiveEffects);
    this.activeEvent.roll = attackValue;
    return this.activeEvent.occurs;
  }

  /**
   * applys the specified skills effects to gamestate
   * subtracts cost
   * adds skill to used skills
   * @param skill
   * @returns undefined
   */
  applySkill(skill: Skill) {
    if (this.activeEvent == undefined) {
      return;
    } else if (skill.isProbability == false &&
       skill.checkFor(this.activeEvent)) {
      switch (skill.operation) {
        case 'ADD':
          this.activeEvent.cost += skill.effectValue;
          break;
        case 'SUB':
          this.activeEvent.cost -= skill.effectValue;
          if (this.activeEvent.cost < 0) {
            this.activeEvent.cost = 0;
          }
          break;
        case 'SET':
          this.activeEvent.cost = skill.effectValue;
          break;
      }
    }

    skill.effects?.forEach((effect)=>this.activeEffects.addEffect(effect));
    if (skill.playPrice) {
      this.decreaseMoney(skill.playPrice);
    }
    this.skills.addToUsedSkills(skill);
  }

  /** calls increase- or decreaseMoney for all incomeValues from effects*/
  applyMoneyEffects() {
    let sum = 0;
    this.activeEffects.incomeValues.forEach((amount) => {
      sum += amount;
    });

    if (sum >= 0) {
      this.increaseMoney(sum);
    } else {
      this.decreaseMoney(-sum);
    }
  }

  /** set next event to active event,
   * take next upcoming event and set it to active.
  */
  nextEvent(): Event | undefined {
    // * Deploy new Event
    this.turn++;
    if (this.upcomingEvents) {
      const eventName = this.upcomingEvents.pop();
      if (eventName) {
        const event = this.data.getEvent(eventName);
        this.activeEvent = event;
        return event;
      }
    }
    this.activeEvent = undefined;
    return undefined;
  }

  /** show finished page **/
  showFinishedPage() {
    this.page = Page.finished;
  }

  /** show loading page **/
  showLoadingPage() {
    this.page = Page.loading;
  }
  /**
   * start next turn in game
   * apply incomeValues to money
   * select next event
   * clear usedSkills
   * able
   */
  nextTurn() {
    this.history.addTurn(this);
    this.skills.clearUsedSkills();
    this.activeEffects.tick();
    if (this.nextEvent()) {
      this.applyMoneyEffects();
      if ((this.money<0)) {
        this.history.createEventMap(this);
        setTimeout(() => this.showFinishedPage(), 130);
      }
    } else {
      this.history.createEventMap(this);
      setTimeout(() => this.showFinishedPage(), 1300);
    }
  }

  /** loads initial effects from data and adds them to effects */
  private loadInitialEffects() {
    this.initialEffects.map((effect) => this.data.getEffect(effect)).
        forEach((effect) => this.activeEffects.addEffect(effect));
  }
  /** Show/Hide event description. **/
  toggleEventDescription() {
    this.showEventDescription = !this.showEventDescription;
  }

  /** toggle probabilitymeter flag */
  toggleProbMeterAnimation() {
    this.probMeterIsMoving = !this.probMeterIsMoving;
  }

  /** gets called after provbabilitymeter stops
   * applies event to state and shows eventdescription
   */
  handleEventActivation() {
    if (this.activeEvent && this.activeEvent.occurs) {
      this.activeEvent.applyTo(this);
    }
    this.toggleEventDescription();
  }

  /** checks if an events required actions are satisfied
   *  @returns true if none exist or required actions are satisfied
   *  @returns false if required actions are not satisfied
   */
  nextTurnEnabled() {
    if ( this.activeEvent?.occurs &&
    this.activeEvent?.requiredActions != undefined) {
      // check if any of the required skills got played
      for (const entry of this.activeEvent.requiredActions) {
        if (this.skills.usedSkills.includes(entry)) {
          return true;
        }
      }
      return false;
    } else {
      return true;
    }
  }

  /** Start with default values **/
  startGame() {
    this.page = Page.game;
    this.addUpcomingEvents();
    this.skills.addSkillCards(this.initialSkills);
    this.loadInitialEffects();
    this.shop.refillShop(this.data);
    this.nextTurn();
  }

  /** show levelselection page **/
  showLevelSelectionPage() {
    this.page = Page.levelselection;
  }

  /** Set the state initialized and ready to use **/
  init() {
    this.page = Page.explain;
  }
}
