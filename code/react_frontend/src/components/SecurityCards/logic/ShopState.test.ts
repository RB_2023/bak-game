import GameState from './GameState';
import GameData from './GameData';
import ShopState from './ShopState';

import Skill from './cards/Skill';

import {getMocupSkills} from 'src/api/SecurityCards/MocupSecuritySkillsApi';

describe('SecurityCards', () => {
  describe('ShopState', () => {
    test('should remove card from shop', () => {
      const shopState = new ShopState();
      const card = new Skill(getMocupSkills()[0]);
      shopState.cardsInShop.push(card);
      expect(shopState.cardsInShop).toHaveLength(1);
      shopState.removeFromShop(card);
      expect(shopState.cardsInShop).toHaveLength(0);
    });

    test('should fill shop with cards', ()=> {
      const shopState = new ShopState();
      const gameData = new GameData();
      const card = new Skill(getMocupSkills()[0]);
      gameData.skills.set(card.name, card);
      shopState.shopableSkills = [];
      shopState.shopableSkills.push(card.name);
      expect(shopState.cardsInShop).toHaveLength(0);
      shopState.refillShop(gameData);
      expect(shopState.cardsInShop).toHaveLength(shopState.shopableSkills.length);
    });

    test('should handle card selection', () => {
      const shopState = new ShopState();
      const card = new Skill(getMocupSkills()[0]);
      shopState.handleCardSelect(card);
      expect(shopState.selectedSkill).toEqual(card);
      shopState.handleCardSelect(card);
      expect(shopState.selectedSkill).toEqual(undefined);
    });

    test('should not buy card from shop with insufficient creds', () => {
      const gameState = new GameState();
      const shopState = new ShopState();
      const card = new Skill(getMocupSkills()[0]);
      gameState.money = -10;
      shopState.selectedSkill = card;
      shopState.buy(gameState);
      expect(shopState.insufficientFunds).toBeTruthy();
      expect(shopState.selectedSkill).toEqual(undefined);
    
    });

    test('should buy card from shop with enough creds', () => {
      const gameState = new GameState();
      const shopState = new ShopState();
      const addSkillSpyOn = jest.spyOn(gameState.skills, 'addSkill');
      const applySkillSpyOn = jest.spyOn(gameState, 'applySkill');
      const removeFromShopSpyOn = jest.spyOn(gameState.shop, 'removeFromShop');
      const decreaseMoneySpyOn = jest.spyOn(gameState, 'decreaseMoney');
      const card = new Skill(getMocupSkills()[0]);
      gameState.money = 100;
      shopState.selectedSkill = card;
      shopState.buy(gameState);
      if (card.isAutoPlay) {
        expect(applySkillSpyOn).toBeCalledWith(card);
      } else {
        expect(addSkillSpyOn).toBeCalledWith(card);
      }
      expect(removeFromShopSpyOn).toBeCalledWith(card);
      expect(decreaseMoneySpyOn).toBeCalledWith(card.price);
      expect(shopState.selectedSkill).toEqual(undefined);
    });
  })
})
