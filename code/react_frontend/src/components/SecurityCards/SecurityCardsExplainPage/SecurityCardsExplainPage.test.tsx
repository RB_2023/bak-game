import React from 'react';
import {screen, render, fireEvent} from '@testing-library/react';
import PhishingQuizExplainPage from './SecurityCardsExplainPage';

describe('SecurityCardsExplainPage', () => {
  test('should use clickEvent', () => {
    const click = jest.fn();
    render(<PhishingQuizExplainPage clickEvent={click}/>);
    expect(click).toBeCalledTimes(0);
    fireEvent.click(screen.getByText('OK!'));
    expect(click).toBeCalled();
  });
});
