import React from 'react';
import {render} from '@testing-library/react';
import ProbabilityMeter from './ProbabilityMeter';
import GameState from '../logic/GameState';
import {getMocupEvents} from 'src/api/SecurityCards/MocupSecurityEventsApi';
import {getMocupEffects} from 'src/api/SecurityCards/MocupEffectsApi';
import Event from '../logic/cards/Event';
import { IEffect, IProbabilityEffect } from 'src/api/SecurityCards/EffectsApi';

let gameState:GameState;
let event:Event;
let effects:IEffect[];

describe('SecurityCards', () => {
  describe('ProbabilityMeter', () => {
    beforeEach(() => {
      gameState = new GameState();
      event = new Event(getMocupEvents()[0]);
      gameState.activeEvent = event;
      effects = getMocupEffects();
      gameState.activeEffects.addEffect(effects[0]);
      gameState.activeEffects.addEffect(effects[1]);
      gameState.activeEffects.addEffect(effects[4]);
    })
    
    test('Should Render Positive Effects on isPositive', () => {
      const ref: React.RefObject<ProbabilityMeter> = React.createRef();
      render(<ProbabilityMeter gamestate={gameState} ref={ref} />);
      const bricks = ref.current?.renderBricks(true);
      if(bricks) {
        expect(bricks.length).toBe(2);
        const probability = (effects[0] as IProbabilityEffect).probability;
        expect(bricks[0].props.style.width).toBe(`calc(${probability}% - 5px)`);
      }
    })

    test('Should Render Negative Effects on not isPositive', () => {
      const ref: React.RefObject<ProbabilityMeter> = React.createRef();
      render(<ProbabilityMeter gamestate={gameState} ref={ref} />);
      const bricks = ref.current?.renderBricks(false);
      if(bricks) {
        expect(bricks.length).toBe(1);
        const probability = (effects[4] as IProbabilityEffect).probability;
        expect(bricks[0].props.style.width).toBe(`calc(${-probability}% - 5px)`);
      }
    })
  })
})
