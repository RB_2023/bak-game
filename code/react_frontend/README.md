# BAKGame React Gui

In this repository we provide the react frontend for the Games
We provide multiple games:
 * PhishingQuiz
 * SecurityCards
 * ThreatAttack

## Setup

In order to run this project in development mode the following prequisites are required:

* Node version 16.8.0
* yarn

we recommend the following installation process:

1. install nvm
2. run `nvm install 16.8.0`
3. install yarn with npm: `npm install --global yarn`
4. install the project dependencies with yarn: `yarn`

## To Build or Run this Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

`yarn test --coverage --watchAll` provides a test coverage report for the whole project.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.


### `yarn jsdoc -c jsdoc.conf.json`

This will generate a HTML based documentation about this project, stored at `/docs`
The index of the generated doku is this file.

### `yarn eslint src/**/*.ts*`

This will run eslint manually, notice that eslint is executed on build
aswell. To apply fixes automatically add the `--fix` flag.


## Development workflow

### Documentation
* Jsdocs 
* are created in the doc pipeline step and exported as artefacts
* as short as possible as long as necessary

### Codestyle
* ESlint with google preset
* git precommit hook with autofix option
* Codestyle is checked in Codestyle pipeline stage

for manual stylechecks run:
```
yarn eslint src/**/*.ts*
```
autofix:
```
yarn eslint src/**/*.ts* --fix
```

### Naming guidelines
Files/Folders:
* file and foldernames in PascalCase
* Tsx files are named after their corresponding component.
* Component specific stylesheets are named after the component.
* testfiles share a folder with their component and follow the pattern `Module/ComponentName.test.tsx`

### Git Style
* rebase based workflow
* branch names/commit messages in english
* branch names in PascalCase, e.g. `AddCoolButton`
* commit message schema: first commit message line < 80 chars
```
/folder/(module|file): description

long description
```
to set up pull behaviour:
`git config pull.rebase true`

Git hooks are shared in the repository under `.githooks`
In order to utilize them, the projects githook directory must be reconfigured.
```
git config core.hooksPat .githooks
```
