# Security Cards Dokumentation
## Levels
SecurityCards ist ein Strategiespiel, bei dem die Spieler in die Rolle eines Unternehmens schlüpfen. Es gibt verschiedene Modi, die unterschiedliche Unternehmen und Szenarien darstellen, wie zum Beispiel ein großes Einzelhandelsunternehmen oder ein Ärztehaus. Die verfügbaren Level sind im Ordner ``levels`` unter dem Pfad ``/code/react_frontend/public/SecurityCards/api`` einsehbar:
```
└── levels
│   ├── Aerztehaus.json
│   ├── ExpertenLevel.json
│   ├── Handwerksbetrieb.json
│   ├── ...
```
Unter dem Ordner ``levels`` können beliebige Modi erstellt und hinterlegt werden. Jedes Level ist dabei eine entsprechende JSON-Datei mit folgender Struktur:
```
{
    "name": "Aerztehaus",
    "title": "Ärztehaus Mustermann",
    "subtitle": "",
    "description": ["Kunden", "Gesundheitsdaten", "persönliche Daten"]
}
```
## Events
Innerhalb von Wochen passieren unterschiedliche Events, die die Spieler bewältigen müssen.
Es können **Damaging Events** (Schadensereignisse), wie zum Beispiel ein Cyberangriff, eine Naturkatastrophe oder ein Produktrückruf auftreten. Es können auch **Harmless Events** (Informationsereignisse) oder **Incidents Event** (Vorfallsereignisse) auftreten, die keinen direkten Schaden verursachen, aber dennoch eine Reaktion erfordern. Jedes Event besitzt eine Auftrittswahrscheinlichkeit und besonders bei Damaging Events wird diese abhängig von der aktuellen Resistenz des Spielers beeinflusst. Vor jeder nächsten Woche wählt ein zufälliger Würfelwurf das anstehende Event aus. Dieser Würfelwurf berücksichtigt die aktuelle Resistenzen des Spielers. So ist beispielsweise das Auftreten von bestimmten Damaging Events sehr unwahrscheinlich, wenn bereits Resistenzen im Bezug darauf vorhanden sind. Im Projekt sind die Events unterteilt in die Kategorien und jeweils als alleinstehenden Ordner zusammengefasst unter dem Überordner ``events`` in ``/code/react_frontend/public/SecurityCards/api``:
```
└── events
    ├── damaging
│   │   ├── AttackOnSupplier.json
│   │   ├── AusfallCloudService.json
│   │   ├── AusfallServer.json
│   │   ├── CustomerIssues.json
│   │   ├── ...
│   ├── harmless
│   │   ├── ApplicantFair1.json
│   │   ├── ApplicantFair.json
│   │   ├── CompanyAnniversary.json
│   │   ├── ExpertAngebotITDienstleister.json
│   │   ├── ...
│   └── incidents
│       ├── AngriffEntdeckt.json
│       ├── Diebstahl.json
│       ├── EmailServerVersendetSpam.json
│       ├── ExpertProblemeCloudFS.json
│       ├── ExpertSlowInternet.json
│       ├── ...
```
Im Ordner ``events`` sind die drei verschiedenen Kategorien **Damaging**, **Harmless** und **Incident** jeweils als einzelnes Ordner zusammengefasst. Innerhalb diesen sind die Events mit den dazugehörigen Typen hinterlegt. Es können beliebig weitere Events erstellt und hinterlegt werden. Dazu muss zunächst der Typ des Events berücksichtigt werden, damit dessen Speicherort bestimmt werden kann. 

### Damaging
Ein Damaging Event beschreibt ein Ereignis, bei dem Schaden am Unternehmen verursacht wird. Jedes Damaging Event ist eine entsprechende JSON-Datei mit folgender Struktur:
```
{
    "name": "AttackOnSupplier",
    "label": "Angriff auf einen Zulieferer",
    "description": "Eine wichtige Zulieferfirma hat durch erhebliche Störungen der Firmen-IT Lieferschwierigkeiten.",
    "occurText": "Durch Verzögerungen bei der Auslieferung entstehen Wartezeiten und zusätzliche Kosten.",
    "notOccurText": "",
    "probability": 100,
    "cost": 1000,
    "tags": ["Availablity"],
    "effectNames": ["incomeAttackOnSupplier"]
  }
```
Neben den bereits ersichtlichen Attributen, existieren besondere Werte wie zum Beispiel ``cost``, ``tags`` oder ``effectNames``. Das Attribut ``cost`` beschreibt, wie viel das Auftreten des Damaging Events das Unternehmen finanziell belastet. ``tags`` ist eine Liste von Werten, die beschreiben, welche Eigenschaften der Firma unter dem Schadensereignis betroffen sind. Hier könnte zum Beispiel "Availability" aufgelistet sein, was dazuführt, das mit dem Auftreten eines solchen Schadensereignisses die Verfügbarkeit der Firma beeinträchtigt wird. Das Attribut ``effectNames`` beinhaltet ebenfalls eine Liste mit verschiedenen Namen von Effekten, die im Bezug auf das Damaging Event ausgelöst werden und direkt Einfluss auf das Spiel haben.

### Incidents
Ein Incident Event beschreibt einen Zwischenfall, das negative Auswirkungen auf Personen, Unternehmen oder die Umwelt haben kann. Jedes Incident Event ist eine entsprechende JSON-Datei mit folgender Struktur:
```
{
  "name": "ExpertProblemeCloudFS",
  "label": "Cloud Fileshare Service Probleme",
  "description": "Der Onlinedienst mit dem Sie normalerweise Daten austauschen scheint aktuell nicht zu funktionieren.",
  "occurText": "Der Anbieter des Onlinediensts meldet einen Cybervorfall. Offenbar sind hierbei auch Kundendaten anhanden gekommen.",
  "notOccurText": "Der Anbieter des Onlinediensts hatte wohl eine kurzfristige Unterbrechung der Dienste um eine dringendes Sicherheitsupdate zu installieren.",
  "probability": 50,
  "cost": 2000,
  "tags": ["Data"],
  "effectNames": ["expevent5effect1", "Expevent5effect2"],
  "requiredActions":["ExpertResetPW", "ExpertInformCustomers"],
  "hintText":""
}
```
Zusätzlich zu den bereits offensichtlichen und den identischen Attributen im Vergleich zu einem Damaging Event, verfügt ein Incident Event ebenfalls über das Attribut ``requiredActions``. Dieses beschreibt eine Liste von verschiedenen Aktionen, die im Bezug auf das Incident Event reaktiv eingesetzt werden können. 

### Harmless
Ein Harmless Event ist ein Vorfall, der keine negativen Auswirkungen hat, aber dennoch von Interesse und Bedeutung sein können. Jedes Harmless Event ist eine entsprechende JSON-Datei mit folgender Struktur:
```
{
    "name":"CompanyAnniversary",
    "label": "Firmenjubiläum",
    "description": "Die Firma feiert ihr 50-jähriges Bestehen.",
    "occurText": "Die Geschäftsleitung lädt Mitarbeiter und Geschäftspartner zu einer gemeinsamen Feier ein.",
    "probability": 100,
    "cost": 0,
    "tags": [],
    "effectNames": []
  }
```
Diese Events können dabei ebenfalls bestimmte Effekte auslösen, die unter dem Attribut ``effectNames`` aufgelistet sind.

## Skills
Um mit den Events umzugehen, verfügt der Spieler über verschiedene Skills, die er im Kartenshop kaufen kann. Ein Skill besitzt einen Typ, der dessen Art beschreibt. Beispielsweise ist die Verwendung einer Policy zur Nutzung der 2-Faktor-Authentifizierung ein Skill vom Typ **preventive**. Diese Preventive Skills werden genutzt, um gewisse Maßnahmen vorzubeugen und damit das Auftreten von Schadensereignissen zu minimieren. Es existieren aber auch Skills vom Typ **reactive**. Diese Fähigkeiten können reaktiv gegen entsprechende Ereignisse verwendet werden. Diese Reactive Skills werden für Events verwendet, die eine direkte Reaktion des Spielers zum Zeitpunkt des Events  erfordern.
 Der Kartenshop bietet eine Vielzahl an Karten an, die als Prevention (Skill vom Typ *preventive*) und Reaktion (Skill vom Typ *reactive*) für verschiedene Ereignisse verwendet werden können. Die Spieler starten das Spiel mit einem Kontostand, der die Resourcen ihres Unternehmens darstellt. Neben den Kosten für das Auftreten bestimmter Events, kostet der Kauf von Karten, also Skills, im Shop ebenfalls Geld, weshalb die Spieler sorgfältig auswählen und kaufen müssen, um Insolvenz zu vermeiden. Die Finanzen müssen demnach sorgfältig verwaltet werden, um das Unternehmen erfolgreich durch die Wochen zu führen. Die Skills sind im Ordner ``skills`` unter ``/code/react_frontend/public/SecurityCards/api`` hinterlegt:
```
└── skills
│   ├── 2FAPolicy.json
│   ├── AwarenessSchulung.json
│   ├── BackupRestore.json
│   ├── ...
```
Unter dem Ordner ``skills`` können beliebige weitere Fähigkeiten erstellt und hinterlegt werden. Jeder Skill ist dabei eine entsprechende JSON-Datei mit folgender Struktur:
```
{
  "name":"2FAPolicy",
  "cardType":"preventive",
  "label": "Zwei-Faktor-Authentisierung",
  "description": "Der Zugriff auf unternehmenskritische Accounts wird durch 2 Faktoren (z. B. Passwort + Authenticator-App) geschützt.",
  "effectText": "Erhöht die Resistenz gegen netzwerkbasierte Angriffe.",
  "price": 2500,
  "playPrice": 0,
  "tags": ["Network", "Infrastructure"],
  "effectValue": 0.5,
  "isProbability": true,
  "isAutoPlay":true,
  "operation": "SET",
  "effectNames":["network50-1"],
  "hintText":"Zwei-Faktor-Authentisierung kann Accounts vor unberechtigten Zugriffen schützen, da zusätzlich zum Passwort der 2. Faktor für den Login benötigt wird."
}
```
Zusätzlich zu den offensichtlichen Attributen wie Name etc., besitzt ein Skill die Attribute ``cardType``, ``tags``, ``effectValue``,  ``isAutoPlay``, ``operation`` und ``effectNames``. Das Attribut ``cardType`` beschreibt den Typ des Skills. Dieser ist nötig, um die Karte und deren Spielbarkeit zuordenbar zu machen. ``effectValue`` definiert, welchen Grad an Auswirkung die Karte hat. ``tags`` beschreibt, auf welche Bereiche der Skill direkten Einfluss ausübt. Diese Werte sind es, die die Auftrittswahrscheinlichkeit von bestimmten Damaging und Incident Events direkt beinflussen. ``isAutoPlay`` ist ein Attribut, welches ermöglicht, dass diverse Karten direkt beim Kauf automatisch ausgespielt werden, sodass der Spieler nicht zusätzlich die Karte manuell ausspielen muss. Das optionale Attribut ``operation`` definiert, die Spielbarkeit der Karte. Der Wert "SET" beschreibt, dass der Skill entsprechend ausgespielt werden kann.

## Effects
Im Spiel existieren diverse Effekte, die einen Einfluss auf den Spielablauf haben und bestimmte Ereignisse mit einer gegebenen Wahrscheinlichkeit und einer gegebenen Dauer unterdrücken können. Die Effekte sind im Ordner ``effects`` unter ``/code/react_frontend/public/SecurityCards/api`` hinterlegt: 
```
└── effects
│   ├── backupCosts.json
│   ├── blockBackups.json
│   ├── buyExtendedSupport.json
│   ├── ...
```
Im Ordner ``effects`` können beliebige weitere Effekte erstellt und hinterlegt werden. Jeder Effekt ist dabei eine entsprechende JSON-Datei mit folgender Struktur:
```
{
    "name": "availability10-1",
    "duration": -1,
    "probability":10,
    "tag":"Availability"
}
```
Neben dem Namen des Effektes selbst existieren ebenfalls die Attribute ``duration``, ``probability`` und ``tag``. Das Attribut ``duration`` gibt die Dauer an, in welcher das Effekt aktiv ist. ``proability`` beschreibt eine Wahrscheinlichkeit, zu welcher das Effekt tatsächlich auftritt. Zuletzt definiert  der Wert in ``tag``, welcher Bereich vom Effekt betroffen ist. Beispielsweise führt der Eintrag *Availability* dazu, dass das Effekt direkten Einfluss auf die Verfügbarkeit ausübt.

