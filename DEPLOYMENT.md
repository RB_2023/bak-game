# Deployment Tutorial

## Prerequirements
### Build environment
* Node.js (Version 18 LTS)
* Yarn package manager

In order to setup the build environment install node.js
first. To do so either use your systems package manager
or use the installer under https://nodejs.org/en/download/

Then in order to install yarn, use npm (it should be shipped with node)
to do so run:
```
npm install --global yarn
```
In order to change the node version you may require nvm.
A tutorial how to install it is provided at: https://github.com/nvm-sh/nvm.
Please note that nvm is a user script and therefore usually cannot be installed
via your systems package manager.

To change your node version to 18 run:
```
nvm install 18
```

### Deployment environment
* Webserver with single page application mode (nginx or apache httpd will do)

An example apache httpd config might look like:
```
Options -MultiViews
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.html [QSA,L]

# BROWSER CACHING USING CACHE-CONTROL HEADERS
<ifModule mod_headers.c> 
    # One year for image and video files
    <filesMatch ".(flv|gif|ico|jpg|jpeg|mp4|mpeg|png|svg|swf|webp)$">
        Header set Cache-Control "max-age=31536000, public"
    </filesMatch>

    # One month for JavaScript and PDF files
    <filesMatch ".(js|pdf)$">
        Header set Cache-Control "max-age=2592000, must-revalidate, public"
    </filesMatch>

    # One week for CSS files
    <filesMatch ".(css)$">
        Header set Cache-Control "max-age=604800, must-revalidate, public"
    </filesMatch>
</ifModule>
```

## Building

Change into the `frontend` folder of the repository, install the required
dependencies and create the production build.
```
cd bakgame/code/react_frontend
yarn
yarn build
```
Now an optimized build is provided within `bakgame/code/react_frontend/build`.
We use webpack to build an Node.js free target application, so this output
may be hosted with any Webserver that supports single page applications.

## Deploying
Copy the output of the build job to your httpd root e.g.,
```
cp -r bakgame/code/react_frontend /var/www/html
```

## Deploying only a subset of the games
All BAKGame react games are provided as a single application,
so by default the process of above always deploys all games
that are currently provided within the repository.

In order to deploy only a subset of the games you have to modify
the following files:
* bakgame/code/react_frontend/src/components/Home/Home.tsx
* bakgame/code/react_frontend/src/routing/MainRouter.tsx

Within the `Home.tsx` file delete the links to the games that should not be deployed.

Within the `MainRouter.tsx` file delete the routes to and the imports of the
games that should not be included.


## CI-CD integration
Within this repository we provide a gitlab ci-cd pipeline,
the pipeline is designed to check the build and code style for all
branches and deploy the major game development branches to a test environment
and the master branch to a production system.

### Preparation
In order to use the continues deployment you must configure your server.

**Productive server:**

* The **Productive System** must run the webserver and an ssh server.
* The ssh server must be configured to accept public key authentification.
* The ssh port must be changed to 222 either or this line must be changed in the
provided `.giltab-ci.yml`
* The www root is assumed to be at `~/public_html/`, this might be changed in the
`.gitlab-ci.yml`

**Test server:**

* The **Test System** must run a webserver that supports to serve multiple
instances of the game (we provide a docker-compose file that sets up such
a server at `deployment/development/`). The setup of this system is shown below.
* The ssh server must be configured to accept public key authentification
* The www root is assumed to be at `/deployment/development/deployment/<BranchName>`,
here we distinguish between the branch names so we can deploy more than one state
of the game at once. You may change the path in the `.gitlab-ci.yml`


**Docker test environment:**
To use the docker test environment make sure you have docker and docker-compose
installed. Then copy the `deployment/development` folder to your system and
start it with docker-compose.
```
sudo cp deployment/development/ /deployment/development
cd /deployment/development
sudo docker-compose up
```

### Gitlab Variables
Our deployment requires 4 gitlab-ci variables to be configured in your
repository in order to work.

* `DEPLOY_PROD_HOST`, contains user and host of the deployment system, e.g.,
`tux@deploy.server`.
* `SSH_PRIVAT_DEPLOYMENT`, contains the private key for the ssh access to the
user on the deployment host.
* `TEST_DEPLOYMENT_HOST`, contains the user and host of the test deployment system, e.g.,
`tux@devel.server`.
* `TEST_DEPLOYMENT`, contains the private key for the ssh access to the
user on the test environment.

### Webhooks
You may want to configure webhooks, that trigger a pipeline on the target
branch as soon as something is merged into master or one of the feature branches.
